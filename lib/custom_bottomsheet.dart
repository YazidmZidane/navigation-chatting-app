import 'package:flutter/material.dart';

class CustomButtomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => showModalBottomSheet(
        context: context,
        builder: (context) => _buildButtomSheet(context),
      ),
      child: const Icon(Icons.add_call),
    );
  }

  Container _buildButtomSheet(BuildContext context) {
    return Container(
      height: 400,
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(48),
      ),
      child: ListView(
        children: [
          const ListTile(
            title: Text("Contoh Bottom Sheet"),
          ),
          const TextField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              icon: Icon(Icons.phone),
              labelText: "Masukkan Nomor Telpon",
              prefixText: "+62",
            ),
          ),
          const SizedBox(
            height: 24,
          ),
          Container(
            alignment: Alignment.center,
            child: ElevatedButton.icon(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(Icons.save),
              label: const Text("Save and Close"),
            ),
          ),
        ],
      ),
    );
  }
}
